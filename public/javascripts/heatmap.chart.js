var xmlns = "http://www.w3.org/2000/svg";

var width = 420,
    barHeight = 20;

var x = d3.scale.linear()
    .range([0, width]);

var chart = d3.select(".chart")
    .attr("width", width);


// retrieve RAW data per timeslot
var RAWdata, visdata, MACcount;

// prepare the 2d visual array by counting all the MACs per box
// (0,0) until (50,50)
// init field of zeroes
function initVisData() {
    visdata = [];
    var i1,i2;
    for (i1=0;i1<50;i1++) {
        visdata[i1] = [];
        for (i2=0;i2<50;i2++) { 
            visdata[i1][i2] = 0;
        }
    }
}



d3.json("/javascripts/testdata.json", function(error, json) {
  if (error) return console.warn(error);
  RAWdata = json;
  // alert("fetched");
  // visualizeit();
  
  initVisData();
  
  _.forEach(RAWdata,function(entry, key) {
    visdata[ Math.floor(entry.x) ][ Math.floor(entry.y) ] ++;
    //protip: risky business for (x,y) that does not exist in visdata
  });
  
  MACcount= _.uniq(_.map(RAWdata,"mac")).length;

    var width = 500,height=500;
    var svg = $("svg").prop({
    width:"500", height:"500",
     viewBox:"0 0 500 500"
    });
    
    
    var boxWidth = 500;
    var boxHeight = 500;

    var svgElem = document.createElementNS (xmlns, "svg");
    svgElem.setAttributeNS (null, "viewBox", "0 0 " + boxWidth + " " + boxHeight);
    svgElem.setAttributeNS (null, "width", boxWidth);
    svgElem.setAttributeNS (null, "height", boxHeight);
    svgElem.style.display = "block";
    
    // add squares with the right opacity
    var i1,i2;
    for (i1=0;i1<50;i1++) {
        
        for (i2=0;i2<50;i2++) { 
            
            
            var rect = document.createElementNS (xmlns, "rect");
            rect.setAttributeNS (null, "opacity", visdata[i1][i2]/MACcount + "");
            rect.setAttributeNS (null, "width", "10px");
            rect.setAttributeNS (null, "height", "10px");
            rect.setAttributeNS (null, "x", i1*10+"px");
            rect.setAttributeNS (null, "y", i2*10+"px");
            svgElem.appendChild(rect);
  
            
        }
    }
    
    document.querySelector("p").appendChild(svgElem);

    
    

    
    var x = d3.scale.linear()
    .domain([0,MACcount])
    .range([0, 100]);
  
  // var chart = d3.select(".chart")
    // .attr("width", width)
    // .attr("height", height);
    
    // var bar = chart.selectAll("g")
    // .data(visdata)
  // .enter().append("g")
    // .attr("transform", function(d, i) { return "translate(," + i * barHeight + ")"; });
    // .attr("transform", function(d, i) { return "translate(<x> [<y>])"; });



});

// start counting where all positions are, RAW data needed

// run through the retrieved data, and 

// render all boxes into an SVG, 
    

    
    
    
    