var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var db = mongoose.connection;
mongoose.connect('mongodb://go84:go84@192.168.101.125/test');

db.on('error', console.error);
db.once('open', function() {
  console.log('connection open');
});
db.on('close', function() {
    console.log('disconnected');
});

var sniffSchema = new mongoose.Schema({timestamp: String, mac: String, rssi0: String, rssi1: String, sniffer: String});
var Sniff = mongoose.model('Sniff', sniffSchema);

var positionSchema = new mongoose.Schema({timestamp: Number, mac: String, x: Number, y: Number});
var Position = mongoose.model('Location', positionSchema);

/* PARAMS */
router.param('mac', function (req, res, next, mac) {
  req.mac = mac;
  next();
});

/* GET macs in position db */
router.get('/position/macs', function(req, res) {
  console.log('Request to /api/position/macs');

  Position.distinct('mac')
          //.sort('timestamp')
          .exec((err, output) => {
            if (err) return console.error(err);
            res.json(output);
          });
});


/* GET position entries for a mac */
router.get('/position/mac/:mac', function(req, res) {
  console.log('Request to /api/position/mac for mac: ', req.mac);

  Position.find({mac: req.mac})
          //.sort('timestamp')
          .exec((err, output) => {
            if (err) return console.error(err);
            res.json(output);
          });
});

module.exports = router;
