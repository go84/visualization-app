var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'GO eighty FO', file: 'index' });
});
router.get('/heatmap', function(req, res, next) {
  res.render('index', { title: 'GO eighty FO heatmap', file: 'heatmap' });
});

module.exports = router;
